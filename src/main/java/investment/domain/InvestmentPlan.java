package investment.domain;

import lombok.Value;

import java.util.List;

@Value
public class InvestmentPlan {
    List<FundInInvestment> funds;
    Money amountLeft;
}
