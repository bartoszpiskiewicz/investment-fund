package investment.domain;

import lombok.Value;

@Value
class Fund {
    long id;
    FundKind kind;
    String name;
}
