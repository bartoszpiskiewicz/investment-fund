package investment.domain;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class FundInInvestment {

    long id;
    String kind;
    String name;
    Money amount;
    BigDecimal amountPercentage;

}
