package investment.domain;

import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

class InvestmentComputator {

    private static final InvestmentComputator AGGRESIVE = new InvestmentComputator("0.4", "0.2", "0.4");
    private static final InvestmentComputator SAFE = new InvestmentComputator("0.2", "0.75", "0.05");
    private static final InvestmentComputator BALANCED = new InvestmentComputator("0.3", "0.6", "0.1");
    private static final BigDecimal ONE_HUNDRED = new BigDecimal("100");

    private final BigDecimal polish;
    private final BigDecimal foreign;
    private final BigDecimal cash;

    static InvestmentComputator getComputator(InvestmentType type) {
        InvestmentComputator computator = null;
        switch (type) {
            case SAFE:
                computator = SAFE;
                break;
            case BALANCED:
                computator = BALANCED;
                break;
            case AGGRESIVE:
                computator = AGGRESIVE;
                break;
        }
        return computator;
    }

    private InvestmentComputator(String polish, String foreign, String cash) {
        this.polish = new BigDecimal(polish);
        this.foreign = new BigDecimal(foreign);
        this.cash = new BigDecimal(cash);
    }

    List<FundInInvestment> computePolish(Money amount, List<Fund> polishFunds) {
        BigDecimal fundAmount = amount.getAmount().multiply(polish);
        return compute(fundAmount, polishFunds, polish);
    }

    List<FundInInvestment> computeForeign(Money amount, List<Fund> foreignFunds) {
        BigDecimal fundAmount = amount.getAmount().multiply(foreign);
        return compute(fundAmount, foreignFunds, foreign);
    }

    List<FundInInvestment> computeCash(Money amount, List<Fund> cashFunds) {
        BigDecimal fundAmount = amount.getAmount().multiply(cash);
        return compute(fundAmount, cashFunds, cash);
    }

    private List<FundInInvestment> compute(BigDecimal amountToDispatch, List<Fund> funds, BigDecimal percentagePerFundKind) {
        BigDecimal amount = amountToDispatch.setScale(0, RoundingMode.FLOOR);
        BigDecimal numberOfFunds = new BigDecimal(funds.size());
        BigDecimal amountPerFund = amount.divide(numberOfFunds, 0, BigDecimal.ROUND_FLOOR);
        Modifier modifier = computeAmountModifiersFirstFund(amount, numberOfFunds, amountPerFund);
        return funds.stream()
                .map(fund -> createFundInInvestment(fund, amountPerFund.add(modifier.getModifier()), percentagePerFundKind, amount))
                .collect(Collectors.toList());
    }

    private FundInInvestment createFundInInvestment(Fund fund, BigDecimal fundAmount, BigDecimal percentagePerFundKind, BigDecimal totalAmount) {
        Money money = new Money(fundAmount, "PLN");
        final BigDecimal percentage = computePercentage(percentagePerFundKind, fundAmount, totalAmount);
        return new FundInInvestment(fund.getId(), fund.getKind().name(), fund.getName(), money, percentage);
    }

    private BigDecimal computePercentage(BigDecimal percentagePerFundKind, BigDecimal fundAmount, BigDecimal totalAmount) {
        BigDecimal fundPart = totalAmount.divide(fundAmount, 2, RoundingMode.HALF_UP);
        return percentagePerFundKind.multiply(ONE_HUNDRED)
                .divide(fundPart, 2, RoundingMode.FLOOR);
    }

    private Modifier computeAmountModifiersFirstFund(BigDecimal amount, BigDecimal numberOfFunds, BigDecimal amountPerFund) {
        return new Modifier(amount.subtract(numberOfFunds.multiply(amountPerFund)));
    }

    @AllArgsConstructor
    private static class Modifier {
        private BigDecimal amountForFirstElement;
        BigDecimal getModifier() {
            BigDecimal modifier = amountForFirstElement;
            amountForFirstElement = BigDecimal.ZERO;
            return modifier;
        }
    }
}