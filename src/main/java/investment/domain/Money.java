package investment.domain;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class Money {

    public static final Money ZERO_PLN = new Money(BigDecimal.ZERO, "PLN");
    
    BigDecimal amount;
    String currency;

}
