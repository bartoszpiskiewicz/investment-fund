package investment.domain;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class InvestmentFactory {

    private static InvestmentFactory instance = new InvestmentFactory();

    static InvestmentFactory instance() {
        return instance;
    }

    public static Investment investment(Money money) {
        return new Investment(InvestmentFactory.instance().fundRepository(), money);
    }

    public FundRepository fundRepository() {
        throw new NotImplementedException();
    }
}
