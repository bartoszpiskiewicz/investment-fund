package investment.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Investment {

    private final FundRepository fundRepository;
    private final Money amount;

    InvestmentPlan compute(InvestmentType investmentType) {
        InvestmentComputator computator = InvestmentComputator.getComputator(investmentType);
        List<FundInInvestment> polishFunds = computator.computePolish(amount, fundRepository.findByKind(FundKind.POLISH));
        List<FundInInvestment> foreignFunds = computator.computeForeign(amount, fundRepository.findByKind(FundKind.FOREIGN));
        List<FundInInvestment> cashFunds = computator.computeCash(amount, fundRepository.findByKind(FundKind.CASH));
        List<FundInInvestment> investments = gatherFunds(polishFunds, foreignFunds, cashFunds);
        return new InvestmentPlan(investments, moneyLeft(investments));
    }

    private List<FundInInvestment> gatherFunds(List<FundInInvestment> polishFunds, List<FundInInvestment> foreignFunds, List<FundInInvestment> cashFunds) {
        List<FundInInvestment> investments = new ArrayList<>(polishFunds);
        investments.addAll(foreignFunds);
        investments.addAll(cashFunds);
        return Collections.unmodifiableList(investments);
    }

    private Money moneyLeft(List<FundInInvestment> investments) {
        BigDecimal sum = investments.stream()
                .map(FundInInvestment::getAmount)
                .map(Money::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return new Money(amount.getAmount().subtract(sum),"PLN");
    }

}
