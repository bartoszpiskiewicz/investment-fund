package investment.domain;

import java.util.List;

public interface FundRepository {

    List<Fund> findByKind(FundKind kind);

}
