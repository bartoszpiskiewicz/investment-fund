package investment.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryFundRepository implements FundRepository {

    private List<Fund> funds;

    public void fillWithStage1Data() {
        funds = new ArrayList<>();
        funds.add(new Fund(1, FundKind.POLISH, "Fundusz Polski 1"));
        funds.add(new Fund(2, FundKind.POLISH, "Fundusz Polski 2"));

        funds.add(new Fund(3, FundKind.FOREIGN, "Fundusz Zagraniczny 1"));
        funds.add(new Fund(4, FundKind.FOREIGN, "Fundusz Zagraniczny 2"));
        funds.add(new Fund(5, FundKind.FOREIGN, "Fundusz Zagraniczny 3"));

        funds.add(new Fund(6, FundKind.CASH, "Fundusz Pieniężny 1"));
    }

    public void fillWithStage2Data() {
        funds = new ArrayList<>();
        funds.add(new Fund(1, FundKind.POLISH, "Fundusz Polski 1"));
        funds.add(new Fund(2, FundKind.POLISH, "Fundusz Polski 2"));
        funds.add(new Fund(3, FundKind.POLISH, "Fundusz Polski 3"));

        funds.add(new Fund(4, FundKind.FOREIGN, "Fundusz Zagraniczny 1"));
        funds.add(new Fund(5, FundKind.FOREIGN, "Fundusz Zagraniczny 2"));

        funds.add(new Fund(6, FundKind.CASH, "Fundusz Pieniężny 1"));
    }

    @Override
    public List<Fund> findByKind(FundKind kind) {
        return funds.stream()
                .filter(fund -> fund.getKind() == kind)
                .collect(Collectors.toList());
    }
}
