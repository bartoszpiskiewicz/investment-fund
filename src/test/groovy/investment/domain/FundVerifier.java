package investment.domain;

import java.math.BigDecimal;
import java.util.List;

public class FundVerifier {

    private static BigDecimal ONE_HUNDRED = new BigDecimal("100");

    static boolean verifyDispatch(List<FundInInvestment> funds) {
        return isFundProper(funds.get(0), 1L, "1000", "0.1")
                && isFundProper(funds.get(1), 2L, "1000", "0.1")
                && isFundProper(funds.get(2), 3L, "2500", "0.25")
                && isFundProper(funds.get(3), 4L, "2500", "0.25")
                && isFundProper(funds.get(4), 5L, "2500", "0.25")
                && isFundProper(funds.get(5), 6L, "500", "0.05");
    }

    public static boolean verifyDispatchForOtherData(List<FundInInvestment> funds) {
        return isFundProper(funds.get(0), 1L, "668", "0.0668")
                && isFundProper(funds.get(1), 2L, "666", "0.0666")
                && isFundProper(funds.get(2), 3L, "666", "0.0666")
                && isFundProper(funds.get(3), 4L, "3750", "0.375")
                && isFundProper(funds.get(4), 5L, "3750", "0.375")
                && isFundProper(funds.get(5), 6L, "500", "0.05");
    }

    private static boolean isFundProper(FundInInvestment fund, long id, String amount, String ratio) {
        BigDecimal percentage = new BigDecimal(ratio).multiply(ONE_HUNDRED);
        return fund.getId() == id && fund.getAmount().getAmount().compareTo(new BigDecimal(amount)) == 0
                && fund.getAmount().getCurrency().equals("PLN") && fund.getAmountPercentage().compareTo(percentage) == 0;
    }

}
