package investment.domain

import spock.lang.Specification

class InvestmentComputationTest extends Specification {

    Money amount
    InvestmentType investmentType
    InMemoryFundRepository repository

    Investment _investment

    InvestmentPlan _plan

    def "should compute investment"() {
        given:
            fund()
            anAmount("10000")
            investType()
            investment()
        when:
            compute()

        then:
            noMoneyLeft()
            amountProperlyDispatched()
    }

    def "should compute investment and left amount"() {
        given:
            fund()
            anAmount("10001")
            investType()
            investment()
        when:
            compute()
        then:
            moneyLeft("1")
            amountProperlyDispatched()
    }

    def "should compute investment with other data set"() {
        given:
        fundWithOtherData()
        anAmount("10000")
        investType()
        investment()
        when:
        compute()
        then:
        noMoneyLeft()
        amountProperlyDispatchedForOtherSet()
    }

    def fund() {
        repository = new InMemoryFundRepository()
        repository.fillWithStage1Data()
    }

    def fundWithOtherData() {
        repository = new InMemoryFundRepository()
        repository.fillWithStage2Data()
    }

    def anAmount(String val) {
        amount = new Money(new BigDecimal(val), "PLN")
    }

    def investType() {
        investmentType = InvestmentType.SAFE
    }

    def investment() {
        _investment = new Investment(repository, amount)
    }

    def compute() {
        _plan = _investment.compute(investmentType)
    }

    def noMoneyLeft() {
        _plan.amountLeft == zeroPLN()
    }

    def moneyLeft(String amount) {
        _plan.amountLeft.amount == new BigDecimal(amount)
    }

    def amountProperlyDispatched() {
        _plan.funds.size() == 6 && FundVerifier.verifyDispatch(_plan.funds)
    }

    Object amountProperlyDispatchedForOtherSet() {
        _plan.funds.size() == 6 && FundVerifier.verifyDispatchForOtherData(_plan.funds)
    }

    def zeroPLN() {
        new Money(BigDecimal.ZERO, "PLN")
    }

}
