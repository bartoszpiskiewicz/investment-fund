package investment.domain

import spock.lang.Specification

class InvestmentCreationTest extends Specification {

    Money amount

    Investment _investment

    InvestmentPlan _plan

    def "should create investment for given amount"() {
        given:
            anAmount()

        when:
            createInvestment()

        then:
            investmentCreated()
    }

    def "should create investment plan"() {
        given:
            anAmount()
            createInvestment()

        when:
            invest(InvestmentType.SAFE)

        then:
            investmentPlanCreated()
    }

    def anAmount() {
        amount = new Money(new BigDecimal("10000"), "PLN")
    }

    def createInvestment() {
        def repository = new InMemoryFundRepository()
        repository.fillWithStage1Data()
        _investment = new Investment(repository, amount)
    }

    def investmentCreated() {
        _investment != null
    }

    def invest(InvestmentType investmentType) {
        _plan = _investment.compute(investmentType)
    }

    def investmentPlanCreated() {
        _plan != null
    }
}
