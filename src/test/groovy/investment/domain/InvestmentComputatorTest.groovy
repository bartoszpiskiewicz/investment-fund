package investment.domain

import spock.lang.Specification

class InvestmentComputatorTest extends Specification {

    Money amount
    InvestmentType investmentType
    FundRepository repository

    InvestmentComputator _computator

    List<FundInInvestment> _funds

    def "should compute investment"() {
        given:
        fundsData()
        anAmount()
        investType(InvestmentType.SAFE)
        computator()
        when:
        compute()

        then:
        amountProperlyDispatched()
    }

    def fundsData() {
        def repository = new InMemoryFundRepository()
        ((InMemoryFundRepository)repository).fillWithStage1Data()
        this.repository = repository
    }

    def anAmount() {
        amount = new Money(new BigDecimal("10000"), "PLN")
    }

    def investType(InvestmentType type) {
        investmentType = type
    }

    def computator() {
        _computator = InvestmentComputator.getComputator(investmentType)
    }

    def compute() {
        def polish = _computator.computePolish(amount, fundsFor(FundKind.POLISH))
        def foregn = _computator.computeForeign(amount, fundsFor(FundKind.FOREIGN))
        def cash = _computator.computeCash(amount, fundsFor(FundKind.CASH))
        polish.addAll(foregn)
        polish.addAll(cash)
        _funds = polish
    }

    def amountProperlyDispatched() {
        _funds.size() == 6 && FundVerifier.verifyDispatch(_funds)
    }

    private def fundsFor(FundKind kind) {
        repository.findByKind(kind)
    }


}
