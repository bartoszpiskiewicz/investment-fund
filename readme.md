#Investment Computator

To compute investment you have to implement FundRepository to provide funds data.
To obtain Investment object use InvestmentFactory. Write following code: 
```java
    InvestmentPlan = InvestmentFactory.investment(new Money("10000", "PLN"))
        .compute(InvestmentType.SAFE);

```

InvestmentPlan is an object with data about investments (amount left and amount stored in each fund).



